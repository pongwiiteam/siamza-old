package com.saimza.radio;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.MediaController;
import android.widget.VideoView;

public class new1 extends AppCompatActivity {
    VideoView mVideoStreamView;
    String mVideoURL="http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new1);

       // mVideoURL = getIntent().getStringExtra("EXT_URL");
        mVideoStreamView = (VideoView) findViewById(R.id.videoView);

        MediaController mc = new MediaController(this);


        mVideoStreamView.setVideoURI(Uri.parse(mVideoURL));
        mVideoStreamView.setMediaController(mc);
        mVideoStreamView.requestFocus();
        mVideoStreamView.start();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_new1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

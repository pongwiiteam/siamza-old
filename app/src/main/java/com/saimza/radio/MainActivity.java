package com.saimza.radio;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements ActionBar.TabListener {

    SectionsPagerAdapter mSectionsPagerAdapter;

    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Set up the action bar.
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {

            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return "GOLF1".toUpperCase(l);
                case 1:
                    return "GOLF2".toUpperCase(l);
                case 2:
                    return "GOLF3".toUpperCase(l);
            }
            return null;
        }
    }

    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";
        int position;

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        ListView myListView;
        TextView txt_test;
        String[] srtListView;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            position = getArguments().getInt(ARG_SECTION_NUMBER);
            View rootView = null;

            switch (position) {
                case 0:
                    rootView = getFirstView1(inflater, container);

                    break;

                case 1:
                    rootView = getFirstView2(inflater, container);

                    break;

                case 2:
                    rootView = getFirstView3(inflater, container);

                    break;
            }
            Log.e("position", String.valueOf(position));
            return rootView;
        }//end onCreateView

        public View getFirstView1(LayoutInflater inflater, ViewGroup container) {
            View myView = inflater.inflate(R.layout.layout1, container, false);

            myListView = (ListView) myView.findViewById(R.id.myListView);
            srtListView = getResources().getStringArray(R.array.my_data_List);

            ArrayAdapter<String> objAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_list_item_1, srtListView);
            myListView.setAdapter(objAdapter);

//            txt_test = (TextView)myView.findViewById(R.id.section_label);
//            txt_test.setText("ONE");
            myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent;
                    switch (position) {
                        case 0:
                            intent = new Intent(getActivity(), new1.class);
                            startActivity(intent);
                            break;
                        case 1:
                            intent = new Intent(getActivity(), new2.class);
                            startActivity(intent);
                            break;
                        case 2:
                            intent = new Intent(getActivity(), new3.class);
                            startActivity(intent);
                            break;

                    }

                }
            });

            return myView;
        }//end getFirstView1

        CustomAdapter adapterView2;
        ListView listView2;
        String[] club;
        RelativeLayout golf;
        Spinner Stoptime;
        MainActivity main;
        Notification myNotification;
        GridView gridview;
        public View getFirstView2(LayoutInflater inflater, ViewGroup container) {
            View myView = inflater.inflate(R.layout.layout2, container, false);

            main = (MainActivity)this.getActivity();
            //listView2 = (ListView) myView.findViewById(R.id.listView1);
             gridview = (GridView) myView.findViewById(R.id.gridview);
            golf = (RelativeLayout) myView.findViewById(R.id.golf);
            Stoptime = (Spinner) myView.findViewById(R.id.spinner);
            club = getResources().getStringArray(R.array.club);

            ArrayAdapter<String> adapterclub = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, club);
            Stoptime.setAdapter(adapterclub);

            new LoadNewsContentView2().execute();


            return myView;
        }//end getFirstView2

        public Handler handler;
        public List<YoutubeItem> results;

        public View getFirstView3(LayoutInflater inflater, ViewGroup container) {
            View myView = inflater.inflate(R.layout.layout3, container, false);

            handler = new Handler();
            loadYoutubeItem(myView);

            return myView;
        }//end getFirstView3

        private void loadYoutubeItem(final View myView) {
            new Thread() {
                public void run() {
                    YoutubeConnector yc = new YoutubeConnector(getActivity(), "LLL-NJEFGcDqLNhH_cNCrfAg"); //playlistId
                    results = yc.getYoutubeItem();
                    handler.post(new Runnable() {
                        public void run() {
                            onGetItemSuccess(myView);
                        }
                    });
                }
            }.start();
        }

        public void onGetItemSuccess(View myView) {

            String[] image = new String[results.size()];
            String[] title = new String[results.size()];
            final String[] video_id = new String[results.size()];
            for (int i = 0; i < results.size(); i++) {

                title[i] = results.get(i).getTitle();
                image[i] = results.get(i).getThumbnailURL();
                video_id[i] = results.get(i).getVideoId();

//                Log.e("POSITION",String.valueOf(i));
//                Log.e("Title",results.get(i).getTitle());
//                Log.e("Image",results.get(i).getThumbnailURL());
//                Log.e("VideoID",results.get(i).getVideoId());
            }

            CustomAdapter adapter = new CustomAdapter(getActivity(), title, image, video_id);

            ListView listView = (ListView) myView.findViewById(R.id.listView1);
            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Intent intent = new Intent(getActivity(), you2.class);
                    intent.putExtra("id", video_id[position]);
                    startActivity(intent);
                }
            });

        }//   Youtube

        public class LoadNewsContentView2 extends AsyncTask<String, String, String> {
            JSONArray row_json = null;
            //private String url = "http://app.siamza.com/get_radio.php?cate_id=1";
            JSONParser jParser = new JSONParser();
            String[] radio_link, radio_name, radio_icon;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                //เข้าแรกสุด
            }

            protected String doInBackground(String... args) {

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("cate_id", "1"));
                JSONObject json = jParser.makeHttpRequest("http://app.siamza.com/get_radio.php", "GET", params);
                Log.e("test : ", json.toString());
                try {
                    row_json = json.getJSONArray("radio");
                    radio_link = new String[row_json.length()];
                    radio_name = new String[row_json.length()];
                    radio_icon = new String[row_json.length()];

                    for (int x = 0; x < row_json.length(); x++) {

                        JSONObject obj_json = row_json.getJSONObject(x);
                        //Log.e("x ", String.valueOf(row_json.length()));

                        radio_link[x] = obj_json.getString("radio_link");
                        radio_name[x] = obj_json.getString("radio_name");
                        radio_icon[x] = obj_json.getString("radio_icon");
//                        Log.e("POSITION",String.valueOf(x));
//                        Log.e("radio_link ", radio_link[x].toString());
//                        Log.e("radio_name ", radio_name[x].toString());
//                        Log.e("radio_icon ", radio_icon[x].toString());
                        //ถ้าเชื่อมต่อกับ server ไม่ได้จะทำงานต่อไปนี้
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            MediaPlayer mPlayer;
            Handler handler;
            Runnable runnable;
            EditText editText1;
            String[] radio_link1, radio_name1, radio_icon1;
            ImageButton Stop;

            int textlength;

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);


                editText1 = (EditText) getActivity().findViewById(R.id.editText1);
                Stop = (ImageButton) getActivity().findViewById(R.id.buttonStop);
                adapterView2 = new CustomAdapter(getActivity(), radio_name, radio_icon, radio_link);
                gridview.setAdapter(adapterView2);
                //imageView1.setVisibility(View.GONE);


                editText1.addTextChangedListener(new TextWatcher() {
                    public void afterTextChanged(Editable arg0) {

                        ArrayList<String> src_name = new ArrayList<String>();
                        ArrayList<String> src_icon = new ArrayList<String>();
                        ArrayList<String> src_link = new ArrayList<String>();
                        Log.e("src_name", String.valueOf(src_name));

                        textlength = editText1.getText().length();
                        for (int i = 0; i < radio_name.length; i++) {
                            try {
                                if (editText1.getText().toString()
                                        .equalsIgnoreCase(radio_name[i]
                                                .subSequence(0, textlength)
                                                .toString())) {
                                    src_name.add(radio_name[i]);
                                    src_icon.add(radio_icon[i]);
                                    src_link.add(radio_link[i]);

                                }
                            } catch (Exception e) {
                            }
                        }
                        radio_name1 = src_name.toArray(new String[src_name.size()]); //search listview
                        radio_icon1 = src_icon.toArray(new String[src_icon.size()]);
                        radio_link1 = src_link.toArray(new String[src_link.size()]);
                        adapterView2 = new CustomAdapter(getActivity(), radio_name1, radio_icon1, radio_link1);

                        gridview.setAdapter(adapterView2);
                        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {


                            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                                golf.setVisibility(View.VISIBLE);

                                try {

                                    if (mPlayer.isPlaying()) {

                                        mPlayer.stop();
                                        mPlayer.release();
                                        mPlayer = new MediaPlayer();
                                        mPlayer.setDataSource(radio_link1[position]);
                                        Log.e("radio_link", String.valueOf(radio_link1[position]));
                                        Log.e("radio_name", String.valueOf(radio_name1[position]));
                                        mPlayer.prepareAsync();
                                        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                                            @Override
                                            public void onPrepared(MediaPlayer mp) {
                                                mp.start();
                                            }

                                        });

                                    } else {


                                        mPlayer = new MediaPlayer();
                                        mPlayer.setDataSource(radio_link1[position]);
                                        mPlayer.prepareAsync();
                                        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                            @Override
                                            public void onPrepared(MediaPlayer mp) {
                                                mp.start();

                                            }
                                        });


                                        handler.removeCallbacks(runnable);
                                        Stoptime.setSelection(0);
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                Stoptime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        if (position != 0) {
                                            int delay = 0;

                                            switch (position) {
                                                case 1:
                                                    delay = 5000;
                                                    break;
                                                case 2:
                                                    delay = 10000;
                                                    break;
                                            }
                                            Log.e("position", String.valueOf(position));

                                            handler.postDelayed(runnable, delay);
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                                Stop.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mPlayer.stop();
                                        Log.e("position", String.valueOf(position));
                                    }
                                });
                            }

                        });
                    }

                    public void beforeTextChanged(CharSequence s, int start
                            , int count, int after) {
                    }

                    public void onTextChanged(CharSequence s, int start
                            , int before, int count) {
                    }

                });


                final Spinner Stoptime = (Spinner) getActivity().findViewById(R.id.spinner);
                mPlayer = new MediaPlayer();

                handler = new Handler();
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        mPlayer.stop();
                        mPlayer.reset();
                        mPlayer.release();
                        mPlayer = new MediaPlayer();
                        Log.e("position", String.valueOf(position));
                    }
                };

                gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                        golf.setVisibility(View.VISIBLE);

                        try {

                            if (mPlayer.isPlaying()) {

                                mPlayer.stop();
                                mPlayer.release();
                                mPlayer = new MediaPlayer();
                                mPlayer.setDataSource(radio_link[position]);
                                Log.e("radio_link", String.valueOf(radio_link[position]));
                                Log.e("radio_name", String.valueOf(radio_name[position]));
                                mPlayer.prepareAsync();
                                mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        mp.start();
                                    }
                                });


                                NotificationManager mNotificationManager = (NotificationManager) main.getSystemService(Context.NOTIFICATION_SERVICE);


                                NotificationManager notificationManager = (NotificationManager) main.getSystemService(getActivity().NOTIFICATION_SERVICE);

                                myNotification = new Notification(R.drawable.golff, "Radio ", System.currentTimeMillis());

                                String notificationTitle = "Play";
                                String notificationText = "test";

                                PendingIntent contentIntent = PendingIntent.getActivity(main, 0, new Intent(main, MainActivity.class), 0);

                                Log.e("FLAG_ONGOING_EVENT", String.valueOf(Notification.FLAG_ONGOING_EVENT));
                                myNotification.sound = Uri.parse("android.resource://" + main.getPackageName() + "/" + "drawable/alert");
                                myNotification.defaults |= Notification.DEFAULT_ALL;
                                //myNotification.flags |= Notification.FLAG_ONGOING_EVENT;
                                myNotification.setLatestEventInfo(main.getBaseContext(), notificationTitle, notificationText, contentIntent);

                                mNotificationManager.notify(1, myNotification);


                            } else {


                                mPlayer = new MediaPlayer();
                                mPlayer.setDataSource(radio_link[position]);
                                mPlayer.prepareAsync();
                                mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        mp.start();

                                    }
                                });

                                handler.removeCallbacks(runnable);
                                Stoptime.setSelection(0);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Stoptime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position != 0) {
                                    int delay = 0;

                                    switch (position) {
                                        case 1:
                                            delay = 5000;
                                            break;
                                        case 2:
                                            delay = 10000;
                                            break;
                                    }
                                    Log.e("position", String.valueOf(position));

                                    handler.postDelayed(runnable, delay);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        Stop.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mPlayer.stop();
                                Log.e("position", String.valueOf(position));
                            }
                        });

                    }

                });


            }
        }//end LoadNewsContentView2
    }//end Fragment


}//end main

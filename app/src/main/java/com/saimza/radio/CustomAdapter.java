package com.saimza.radio;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class CustomAdapter  extends BaseAdapter {
    Context mContext;
    String[] strName,strImage,video_id;
    int[] resId;
    int check=0;


    public CustomAdapter(Context context, String[] strName, int[] resId) {
        super();
        this.mContext = context;
        this.strName = strName;
        this.resId = resId;
        check =0;
    }

    public CustomAdapter(Context context, String[] strName, String[] strImage,String[] video_id) {
        super();
        this.mContext = context;
        this.strName = strName;
        this.strImage = strImage;
        this.video_id = video_id;
        check = 1;
    }




    public int getCount() {

        return strName.length;
    }

    public Object getItem(int arg0) {

        return null;
    }

    public long getItemId(int arg0) {

        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater)mContext.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

        View row = mInflater.inflate(R.layout.custom, parent, false);


        TextView textView = (TextView)row.findViewById(R.id.textView1);
        textView.setText(strName[position]);
        ImageView imageView = (ImageView)row.findViewById(R.id.imageView1);

        switch (check){
            case  0:imageView.setBackgroundResource(resId[position]);
                break;
            case  1:
                Glide.with(mContext).load(strImage[position]).into(imageView);

                break;
        }




        return row;
    }
}



package com.saimza.radio;

import android.content.Context;
import android.util.Log;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.PlaylistItemListResponse;
import com.google.api.services.youtube.model.PlaylistItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class YoutubeConnector {
    private YouTube youtube;
    private YouTube.PlaylistItems.List query;

    // ใช้ server key
    public static final String KEY= "AIzaSyBt4kv6bS_XSxIbhgr83MRGLFX5x5ou2kg"; //api key
    public YoutubeConnector(Context context,String PLAYLISTID) {
        youtube = new YouTube.Builder(new NetHttpTransport(),
                new JacksonFactory(), new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest hr) throws IOException {}
                }).setApplicationName("ชื่อแอพพลิเคชั่น").build();

        try{
            query = youtube.playlistItems().list("snippet");
            query.setKey(KEY);
            query.setMaxResults(50l);
            query.setPlaylistId(PLAYLISTID);
            query.setFields("items(snippet/title,snippet/thumbnails/medium/url,snippet/resourceId/videoId)");

        }catch(IOException e){
            Log.e("YoutubeConnector", "Could not initialize : " + e);
        }
    }

    public List<YoutubeItem> getYoutubeItem(){
        try{
            PlaylistItemListResponse response = query.execute();
            Log.e("Youtube",response.toString());
            List<PlaylistItem> results = response.getItems();
            List<YoutubeItem> items = new ArrayList<YoutubeItem>();
            for(PlaylistItem result:results){
                YoutubeItem item = new YoutubeItem();
                
                item.setTitle(result.getSnippet().getTitle());
                item.setThumbnailURL(result.getSnippet().getThumbnails().getMedium().getUrl());
                item.setVideoId(result.getSnippet().getResourceId().getVideoId());
                items.add(item);
            }
            return items;
        }catch(IOException e){
            Log.e("YoutubeConnector", "Could not get item : "+e);
            return null;
        }
    }
}
